# Código del curso JDBC a profundidad + Spring Framework + MySQL + H2
# Instructor
> [Alejandro Agapito Bautista](https://www.udemy.com/course/spring-jdbc/) 
 

# Udemy
* JDBC a profundidad + Spring Framework + MySQL + H2

## Contiene

* Acceder a bases de datos con JDBC
* SQL  (Structured query language)
* JDBC (Java database connectivity)
* Pools de conexiones
* H2 Connection pool
* HickariCP
* C3po
* DBCP2
* Uso de transacciones
* Uso de Spring Jdbc
 

## Descripción
      

---

## Notas
 
~~~
* Repositorio solo en ** Gitlab  **

/************* MAVEN ***********************/
 
 *  


---
## Código 

`//desarrollo` 
 `public int suma( int x, int y ) { return x + y; }` 

 `//test` 
`@Test`
`public int suma( int x, int y ) {  assertEquals(2, ( x + y ) ); }`


 

---
#### Version  V.0.1 
* **V.0.1**  _>>>_  Iniciando proyecto [  on 18 FEB, 2022 ]  
 
 
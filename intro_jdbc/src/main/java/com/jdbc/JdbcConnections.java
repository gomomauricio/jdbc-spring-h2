package com.jdbc;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.h2.tools.RunScript;
 

public class JdbcConnections 
{
	
	public static void main(String[] args) {
		
		try 
		{
			System.out.println("Conectando ...");
			 Connection con = DriverManager.getConnection("jdbc:h2:~/test");
			System.out.println("Conectado");
			
			
			        try 
			        {
			        	
						RunScript.execute(con,new FileReader("src/main/resources/schema.sql"));
						System.out.println("Script executed ...");
						
						PreparedStatement statement = con.prepareStatement("insert into person (name,last_name,nickname)"
								+ " values(?,?,?)");
						
						statement.setString(1, "Mauricio");
						statement.setString(2, "Gonzalez");
						statement.setString(3, "@TicioRotten");
						
						
						int rows = statement.executeUpdate();
						
						System.out.println( "Rows impacted  "  + rows);
						
						statement = con.prepareStatement("Select * from person");
						
						
						ResultSet rs = statement.executeQuery();
						
						while(rs.next())
						{
							System.out.println(" # " + rs.getString(1));
							System.out.println(" # " + rs.getString(2));
							System.out.println(" # " + rs.getString(3));
							System.out.println(" # " + rs.getString(4));
						}
						
						//execute
						statement = con.prepareStatement("Select * from person");
					 	boolean execute = statement.execute();
					 	System.out.println("Is resultSet: " + (execute));
//						
//						statement.exe
						
						statement.close();
						
						System.out.println("Cerrando ...");
						con.close(); 
					} 
			        catch (SQLException | FileNotFoundException e) 
			        {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

			
			
			
			
			System.out.println("Cerrado ...");
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		
	}

}

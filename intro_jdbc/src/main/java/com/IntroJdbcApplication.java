package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntroJdbcApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntroJdbcApplication.class, args);
		
		System.out.println("Iniciando INTRO JDBC . . . ");
	}

}
